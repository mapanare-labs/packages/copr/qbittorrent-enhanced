# Use old cmake macro, handle build dir ourselves
%global __cmake_in_source_build 1

Name:    qbittorrent-enhanced
Summary: qBittorrent fork with various improvements
Version: 4.6.6.10
Epoch:   1

%global forgeurl https://github.com/c0re100/qBittorrent-Enhanced-Edition
%global tag release-%{version}

%forgemeta

Release: 1%{?dist}
License: GPL-2.0-or-later
URL:     %{forgeurl}
Source0: %{forgesource}

BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: ninja-build
BuildRequires: systemd

BuildRequires: cmake(Qt6Core)
BuildRequires: cmake(Qt6Gui)
BuildRequires: cmake(Qt6Svg)
BuildRequires: cmake(Qt6LinguistTools)
BuildRequires: libxkbcommon-devel
BuildRequires: qt6-qtbase-private-devel
BuildRequires: qt6-linguist

BuildRequires: pkgconfig(zlib)
BuildRequires: rb_libtorrent-devel >= 2.0
BuildRequires: desktop-file-utils
BuildRequires: boost-devel >= 1.60
BuildRequires: libappstream-glib

Requires: python3
Conflicts: qbittorrent%{?_isa}

%description
Enhanced version of qBittorrent.
Features:
Auto Ban Xunlei, QQ, Baidu, Xfplay, DLBT and Offline downloader
Auto Ban Unknown Peer from China Option (Default: OFF)
Auto Update Public Trackers List (Default: OFF)
Auto Ban BitTorrent Media Player Peer Option (Default: OFF)
Peer whitelist/blacklist

%package nox
Summary: A Headless Bittorrent Client

%description nox
A headless enhanced version of qBittorrent client using rb_libtorrent and a Qt5.
Features:
Auto Ban Xunlei, QQ, Baidu, Xfplay, DLBT and Offline downloader
Auto Ban Unknown Peer from China Option (Default: OFF)
Auto Update Public Trackers List (Default: OFF)
Auto Ban BitTorrent Media Player Peer Option (Default: OFF)
Peer whitelist/blacklist

%prep
%forgeautosetup -p1

# Remove "enhanced" UA.
sed 's/qBittorrent Enhanced/qBittorrent/g' -i src/base/bittorrent/sessionimpl.cpp
#sed 's/QBT_VERSION_BUGFIX 1/QBT_VERSION_BUGFIX 0/g' -i src/base/version.h.in
#sed 's/QBT_VERSION_BUGFIX 2/QBT_VERSION_BUGFIX 0/g' -i src/base/version.h.in
#sed 's/QBT_VERSION_BUGFIX 3/QBT_VERSION_BUGFIX 0/g' -i src/base/version.h.in
#sed 's/QBT_VERSION_BUGFIX 5/QBT_VERSION_BUGFIX 0/g' -i src/base/version.h.in
sed 's/QBT_VERSION_BUILD 10/QBT_VERSION_BUILD 0/g' -i src/base/version.h.in
sed 's/QBT_VERSION_BUILD 11/QBT_VERSION_BUILD 0/g' -i src/base/version.h.in
#sed 's/qBittorrent Enhanced/qBittorrent/g' -i src/base/net/dnsupdater.cpp

%build
mkdir %{_vpath_builddir}-nox
pushd %{_vpath_builddir}-nox
%cmake \
 -DSYSTEMD=ON \
 -Wno-dev \
 -GNinja \
 -DQT6=ON \
 -DGUI=OFF \
 ..
%cmake_build
popd

# Build gui version
mkdir %{_vpath_builddir}
pushd %{_vpath_builddir}
%cmake \
 -Wno-dev \
 -DQT6=ON \
 -GNinja \
 ..
%cmake_build
popd

%install
# install headless version
pushd %{_vpath_builddir}-nox
%cmake_install
popd

# install gui version
pushd %{_vpath_builddir}
%cmake_install
popd

desktop-file-install \
  --dir=%{buildroot}%{_datadir}/applications/ \
  %{buildroot}%{_datadir}/applications/org.qbittorrent.qBittorrent.desktop

appstream-util validate-relax --nonet %{buildroot}%{_metainfodir}/org.qbittorrent.qBittorrent.appdata.xml

%files
%license COPYING
%doc README.md AUTHORS Changelog
%{_bindir}/qbittorrent
%{_metainfodir}/org.qbittorrent.qBittorrent.appdata.xml
%{_datadir}/applications/org.qbittorrent.qBittorrent.desktop
%{_datadir}/icons/hicolor/*/apps/qbittorrent.*
%{_datadir}/icons/hicolor/*/status/qbittorrent-tray*
%{_mandir}/man1/qbittorrent.1*

%files nox
%license COPYING
%doc AUTHORS Changelog
%{_bindir}/qbittorrent-nox
%{_unitdir}/qbittorrent-nox@.service
%{_mandir}/man1/qbittorrent-nox.1*
